﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZakonKz.Data.Models
{
    public class Publication
    {
        public int Id { get; set; }
        public int ZakonKzId { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }
    }
}
