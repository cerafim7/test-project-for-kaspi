﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System.Configuration;

namespace ZakonKz.Data
{
    public class ContextFactory : IDesignTimeDbContextFactory<Context>
    {
        public Context CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<Context>();

            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=ZakonDB;Trusted_Connection=True;");

            return new Context(optionsBuilder.Options);
        }
    }
}
