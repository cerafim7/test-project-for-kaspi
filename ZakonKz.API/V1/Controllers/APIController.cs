﻿using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using ZakonKz.Data;
using ZakonKz.Data.Models;

namespace ZakonKz.API.V1.Controllers
{
    [Authorize(Roles = "User")]
    [ApiVersion("1.0")]
    [Route("v{api-version:apiVersion}/api")]
    [Produces("application/json")]
    public class APIController: Controller
    {
        private readonly Context _context;

        public APIController(ContextFactory contextFactory)
        {
            _context = contextFactory.CreateDbContext(new string[] { });
            _context.Database.EnsureCreated();
        }

        
        [HttpGet("posts")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<Publication>))]
        [SwaggerResponse((int)HttpStatusCode.NoContent, Type = typeof(void))]
        public async Task<IActionResult> Posts(DateTime from, DateTime to)
        {
            var model = await _context.Publications
                .Where(p => p.Date >= from && p.Date <= to)
                .OrderByDescending(p => p.Date)
                .ToListAsync();

            return Ok(model);
        }

        [HttpGet("topten")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<string>))]
        public async Task<IActionResult> TopTen()
        {
            var model = new List<string>();

            using(var connection = _context.Database.GetDbConnection())
            {
                model = (await connection.QueryAsync<string>(@"
                    SELECT TOP 10
	                    Words.value,
	                    Count(Words.value)
                    FROM 
	                    (
		                    SELECT 
			                    Split.value
		                    FROM
		                    Publications 
		                    CROSS APPLY STRING_SPLIT(Publications.Text, ' ')Split
	                    ) Words
                    GROUP BY
	                    Words.value
                    ORDER BY
	                    COUNT(Words.value) DESC
                ")).ToList();
            }

            return Ok(model);
        }

        [HttpGet("search")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<Publication>))]
        [SwaggerResponse((int)HttpStatusCode.NoContent, Type = typeof(void))]
        public async Task<IActionResult> Search([Required][StringLength(200)]string text)
        {
            var model = await _context.Publications
                .Where(p => p.Title.Contains(text) || p.Text.Contains(text))
                .OrderByDescending(p => p.Date)
                .ToListAsync();

            return Ok(model);

            /*
             Альтернатива - использование полнотекстового поиска, но для этого потребуется предварительная индексация базы данных.
             Пример запроса:
                declare @search nvarchar(200)
                set @words = 'zakon'

                SELECT 
	                FT.RANK,
	                Publications.*
                FROM 
	                Publications
	                INNER JOIN FREETEXTTABLE(Publications, ([Title], [Text]), @search, 1000) AS FT ON Publications.Id = FT.[KEY]
                ORDER BY 
	                FT.RANK DESC, 
	                Publications.[Date] DESC
            */
        }
    }
}
