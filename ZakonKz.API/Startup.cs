﻿using System.Collections.Generic;
using System.Globalization;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Swagger;

namespace ZakonKz.API
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }
        

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<Data.ContextFactory>();

            services.AddResponseCompression(options =>
            {
                options.Providers.Add<GzipCompressionProvider>();
            });

            services.Configure<GzipCompressionProviderOptions>(options =>
            {
                options.Level = CompressionLevel.Fastest;
            });

            services.AddMemoryCache();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);


            var bearerTokenConfig = Configuration.GetSection("BearerToken");
            var bearerTokenIssuer = bearerTokenConfig["Issuer"];
            var bearerTokenAudiences = bearerTokenConfig.GetSection("Audiences").AsEnumerable().Select(e => e.Value);
            var bearerTokenKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(bearerTokenConfig["Key"]));

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateLifetime = false,
                        ValidateIssuer = true,
                        ValidIssuer = bearerTokenIssuer,
                        ValidateAudience = true,
                        ValidAudiences = bearerTokenAudiences,
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = bearerTokenKey,
                    };
                });


            services.AddMvcCore().AddVersionedApiExplorer(
                options =>
                {
                    options.GroupNameFormat = "'v'VVV";
                    options.SubstituteApiVersionInUrl = true;
                });

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
            });

            services.AddApiVersioning(o => o.ReportApiVersions = true);
            services.AddSwaggerGen(
                options =>
                {
                    var provider = services.BuildServiceProvider().GetRequiredService<IApiVersionDescriptionProvider>();
                    foreach (var description in provider.ApiVersionDescriptions)
                    {
                        options.SwaggerDoc(description.GroupName, CreateInfoForApiVersion(description));
                    }
                    options.OperationFilter<SwaggerDefaultValues>();


                    options.DescribeAllEnumsAsStrings();
                    options.AddSecurityDefinition("Bearer", new ApiKeyScheme
                    {
                        Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                        Name = "Authorization",
                        In = "header",
                        Type = "apiKey"
                    });
                    options.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
                    {
                        { "Bearer", Enumerable.Empty<string>() },
                    });
                });
        }


        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApiVersionDescriptionProvider provider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            var cultureInfo = new CultureInfo("ru-RU");
            CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
            CultureInfo.DefaultThreadCurrentUICulture = cultureInfo;

            app.UseAuthentication();
            app.UseMvc();
            app.UseSwaggerAuthorized();
            app.UseSwagger();
            app.UseSwaggerUI(
                options =>
                {
                    var endpointStart = env.IsDevelopment() ? "swagger" : "api/swagger";

                    // build a swagger endpoint for each discovered API version
                    foreach (var description in provider.ApiVersionDescriptions)
                    {
                        options.SwaggerEndpoint($"/{endpointStart}/{description.GroupName}/swagger.json", description.GroupName.ToUpperInvariant());
                    }
                });


        }


        static Info CreateInfoForApiVersion(ApiVersionDescription description)
        {
            var assemblyName = Assembly.GetExecutingAssembly().GetName().Name;
            ResourceManager resourceManager = new ResourceManager($"{assemblyName}.{description.GroupName.ToUpper()}.Resources.Global", Assembly.GetExecutingAssembly());

            var info = new Info()
            {
                Title = resourceManager.GetString("APITitle"),
                Version = description.ApiVersion.ToString(),
                Description = resourceManager.GetString("APIDescription"),
            };

            if (description.IsDeprecated)
            {
                info.Description += " This API version has been deprecated.";
            }

            return info;
        }
    }
}
