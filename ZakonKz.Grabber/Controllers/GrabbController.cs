﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using ZakonKz.Data.Models;

namespace ZakonKz.Grabber.Controllers
{
    public class GrabbController
    {
        public IEnumerable<Publication> GrabbContent(string url)
        {
            XDocument feedXML = XDocument.Load(url);

            var feeds = from feed in feedXML.Descendants("item")
                        select new Publication
                        {
                            Id = 0,
                            ZakonKzId = GetIdFromLink(feed.Element("link").Value),
                            Title = feed.Element("title").Value,
                            Text = feed.Element("description").Value,
                            Date = DateTime.Parse(feed.Element("pubDate").Value)
                        };

            return feeds;
        }

        private int GetIdFromLink(string link)
        {
            if (string.IsNullOrEmpty(link))
            {
                return 0;
            }

            return int.Parse(link.Split('/')[3].Split('-').First());
        }
    }
}
