﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using ZakonKz.Data;
using ZakonKz.Data.Models;

namespace ZakonKz.Grabber.Controllers
{
    class StoreController
    {
        private Context _context;

        public StoreController(string[] args, ContextFactory contextFactory)
        {
            _context = contextFactory.CreateDbContext(args);
            _context.Database.EnsureCreated();
        }

        public void Store(IEnumerable<Publication> publications)
        {
            var publicationZakonIds = publications.Select(p => p.ZakonKzId);
            var storedPublications = _context.Publications
                .Where(p => publicationZakonIds.Contains(p.ZakonKzId))
                .ToList();

            if(storedPublications.Count > 0)
            {
                storedPublications.ForEach(p => p.Id = 0);
                publications = publications.Except(storedPublications);
            }

            _context.Publications.AddRange(publications);
            _context.SaveChanges();
        }
        
    }
}
