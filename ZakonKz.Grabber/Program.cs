﻿using System;
using ZakonKz.Data;
using ZakonKz.Grabber.Controllers;

namespace ZakonKz.Grabber
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("start");

            var grabbController = new GrabbController();
            var publications = grabbController.GrabbContent("http://www.zakon.kz/rss.xml");

            var contextFactory = new ContextFactory();
            var storeController = new StoreController(args, contextFactory);
            storeController.Store(publications);

            Console.WriteLine("end");
        }
    }
}
